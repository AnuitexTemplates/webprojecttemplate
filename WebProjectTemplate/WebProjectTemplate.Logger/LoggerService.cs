﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WebProjectTemplate.Logger.Models;

namespace WebProjectTemplate.Logger
{
	public class LoggerService
	{
		private readonly LoggerSettings _loggerSettings;
		public LoggerService(LoggerSettings loggerSettings)
		{
			_loggerSettings = loggerSettings;
		}

		public int LogInformation(string message)
		{
			using (IDbConnection connection = new SqlConnection(_loggerSettings.ConnectionString))
			{
				var sqlQuery = $"INSERT INTO {_loggerSettings.TableName} ({_loggerSettings.MessageDbColumnName}) VALUES(@Message); SELECT CAST(SCOPE_IDENTITY() as int)";
				var output = connection.Query<int>(sqlQuery, new LogEntry { Message = message }, commandType: CommandType.Text).SingleOrDefault();
				return output;
			}
		}

		public int LogCritical(Exception exception)
		{
			using (IDbConnection connection = new SqlConnection(_loggerSettings.ConnectionString))
			{
				var sqlQuery = $"INSERT INTO {_loggerSettings.TableName} ({_loggerSettings.}) VALUES(@Message); SELECT CAST(SCOPE_IDENTITY() as int)";
				var output = connection.Query<int>(sqlQuery, new LogEntry { Message = exception.Message,ExceptionJson=JsonConvert.SerializeObject(exception) },
					commandType: CommandType.Text).SingleOrDefault();
				return output;
			}
		}
	}
}
