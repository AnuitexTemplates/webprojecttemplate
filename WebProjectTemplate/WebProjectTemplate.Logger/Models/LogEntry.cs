﻿namespace WebProjectTemplate.Logger.Models
{
	public class LogEntry
	{
		public int Id { get; set; }
		public string Message { get; set; }
		public string ExceptionJson { get; set; }
	}
}
