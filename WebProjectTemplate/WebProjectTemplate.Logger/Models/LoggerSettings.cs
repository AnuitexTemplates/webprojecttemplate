﻿namespace WebProjectTemplate.Logger.Models
{
	public class LoggerSettings
	{
		public string ConnectionString { get; private set; }
		public string TableName { get; private set; }
		public string MessageDbColumnName { get; private set; }
		public string ExceptionJsonDbColumnName { get; private set; }

		public LoggerSettings(string connectionString, string tableName, string messageDbColumnName, string exceptionJsonDbColumnName)
		{
			ConnectionString = connectionString;
			TableName = tableName;
			MessageDbColumnName = messageDbColumnName;
			ExceptionJsonDbColumnName = exceptionJsonDbColumnName;
		}
	}
}
