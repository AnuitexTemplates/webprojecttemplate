﻿namespace WebProjectTemplate.BusinessLogic.Services
{
	public class AccountService
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly IEmailSender _emailSender;
		private readonly ILogger _logger;
	}
}
